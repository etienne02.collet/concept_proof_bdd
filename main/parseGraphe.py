from networkx import MultiDiGraph

auto = "G = MultiDiGraph()\n"
G = MultiDiGraph()

print("input line after line all edge at your graphe with format : (key, src, dst, ...label)")
lst_edge = dict()
lst_node = set()
i = 0

while True:
    input1 = input()
    if input1 == "":
        break
    tmp_lst = input1.split()
    if len(tmp_lst) < 4:
        print("value ignored")
        continue
    lbl = set(tmp_lst[3:])
    val = (tmp_lst[1], tmp_lst[2], tmp_lst[0], {"label" : lbl})
    print(str(val))
    lst_node.add(tmp_lst[1])
    lst_node.add(tmp_lst[2])
    lst_edge[tmp_lst[0]] = val

print(lst_edge)
G.add_edges_from(lst_edge.values())
auto += "V = " + str(lst_node) + "\n"
auto += "E = " + str(list(lst_edge.values())) + "\n"
auto += "G.add_nodes_from( V )\n"
auto += "G.add_edges_from( E )\n#print(G.edges(data=True, keys=True))\n"
print(G)
print("\n code : \n\n" + auto)