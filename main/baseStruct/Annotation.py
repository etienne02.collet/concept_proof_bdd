class Annotation:
    """
    before revert
    QA → P(ED × QA) × N
    dict[any, tuple[set[tuple[any, any]], int]]

    after revert :
    QA → N → ED → P(QA)
    """
    def __init__(self) -> None:
        self._data = dict()
        self._revert = False

    def is_revert(self) -> bool:
        return self._revert

    def revert(self) -> None:
        tmp = dict()
        if(self._revert):
            return
        for i in self._data.keys():
            tmp[i] = dict()
            for j in self._data[i].keys():
                tmp[i][j] = dict()
                tmp[i][j][self._data[i][j][1]] = dict()
                for k in self._data[i][j][0]:
                    if k == None:
                        tmp[i][j][self._data[i][j][1]] = None
                        continue
                    if k[0][1] in tmp[i][j][self._data[i][j][1]].keys():
                        tmp[i][j][self._data[i][j][1]][k[0][1]][1] |= {k[1]}
                    else:
                        tmp[i][j][self._data[i][j][1]][k[0][1]] = (k[0][0], {k[1]})
        self._data = tmp
        self._revert = True

    def init_node(self, node: any) -> None:
        self._data[node] = dict()

    def get_dom_to_node(self, node: any) -> set[any]:
        if node not in self._data.keys():
            return {}
        return self._data[node].keys()

    def get_dom_to_state(self, node: any, state: any) -> set[any]:
        if node not in self._data.keys() or state not in self._data[node].keys():
            return {}
        return self._data[node][state].keys()

    def get_val_for_length(self, node: any, state: any, length: int) -> set[tuple[tuple[any, any], any]] | dict[any, tuple[set[any], any]]:
        if self._revert:
            if node in self._data.keys() and state in self._data[node].keys() and length in self._data[node][state].keys():
                return self._data[node][state][length]
        else:
            if node in self._data.keys() and state in self._data[node].keys():
                if self._data[node][state][1] == length:
                    return self._data[node][state][0]
        return None

    def get_val_for_edge(self, node: any, state: any, length: int, edge: any) -> tuple[any, set[any]]:
        if self._revert:
            if node in self._data.keys() and state in self._data[node].keys() and length in self._data[node][state].keys() and edge in self._data[node][state][length].keys():
                return self._data[node][state][length][edge]
        return None

    def get_dom(self) -> set[any]:
        return self._data.keys()

    def add_val(self, node: any, state: any, value: tuple[tuple[any, any], int]) -> bool:
        if self._revert:
            return False
        if node not in self._data.keys():
            self.init_node(node)
        if state in self._data[node].keys():
            if value[1] == self._data[node][state][1]:
                self._data[node][state] = (self._data[node][state][0] | value[0], self._data[node][state][1])
        else:
            if isinstance(value[0], set):
                self._data[node][state] = (value[0], value[1])
            else:
                self._data[node][state] = ({value[0]}, value[1])
        return True

    def get_val(self, node: any, state: any) -> tuple[set[tuple[tuple[any, any], any]], int]:
        if node in self._data.keys() and state in self._data[node].keys():
            return self._data[node][state]
        return None

    def __str__(self) -> str:
        res = ""
        if self._revert:
            for i in self._data.keys():
                res += "" + str(i) + " = " + str(self._data[i]) + " \n"
        else:
            for i in self._data.keys():
                res += "" + str(i) + " = " + str(self._data[i]) + " \n"
        return res
