from __future__ import annotations

class Walk:
    """
    type for display graphe data-base walk
    s is the first node of this walk
    """
    def __init__(self, s: any) -> None:
        self._initial = s
        self._edges = []
        self._nodes = []
        self._size = 0

    def source(self) -> any:
        return self._nodes[self._size - 1] if self._size != 0 else self._initial

    def destination(self) -> any:
        return self._initial

    def size(self) -> int:
        return self._size

    def add(self, edge: any, node: any) -> Walk:
        self._edges.append(edge)
        self._nodes.append(node)
        self._size += 1
        return self

    def supr(self) -> Walk:
        self._edges.pop()
        self._nodes.pop()
        self._size -= 1
        return self

    def __str__(self) -> str:
        walk = ""
        max = len(self._edges)
        for i in range(self._size):
            walk += str(self._nodes[max - 1 - i]) + " -" + str(self._edges[max - 1 - i]) + "-> "
        walk += str(self._initial)
        return walk