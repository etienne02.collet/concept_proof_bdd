class Automaton:
    """
    A = Automaton()
    A.set_graphe([('0', '0', 'A'), ('0', '1', 'B'), ('1', '1', 'A')])
    A.set_initial(['A'])
    A.set_final(['B'])
    print(A.get_alphabet())
    print(A.get_final())
    print(A.get_initial())
    print(A.get_destination('0', 'B'))
    print(A.get_sources('B', '1'))
    """
    def __init__(self) -> None:
        self._initial = set()
        self._final = set()
        self._state = set()
        self._alphabet = set()
        self._sources = dict() # (edge val, state dst) -> state src
        self._dest = dict() # (state src, edge val) -> state dst


    def set_graphe(self, data: list[tuple[any, any, any]]):
        """
        data : (state start, state end, edge value)
        """
        for i in data:
            self._state.add(i[0])
            self._state.add(i[1])
            self._alphabet.add(i[2])
            if (i[2], i[1]) in self._sources.keys():
                self._sources[(i[2], i[1])] |= {i[0]}
            else:
                self._sources[(i[2], i[1])] = {i[0]}
            if (i[0], i[2]) in self._dest.keys():
                self._dest[(i[0], i[2])] |= {i[1]}
            else:
                self._dest[(i[0], i[2])] = {i[1]}

    def set_initial(self, data: list[any]):
        for i in data:
            if i in self._state:
                self._initial.add(i)

    def set_final(self, data: list[any]):
        for i in data:
            if i in self._state:
                self._final.add(i)

    def get_final(self) -> set[any]:
        return self._final

    def get_initial(self) -> set[any]:
        return self._initial

    def get_alphabet(self) -> set[any]:
        return self._alphabet

    def get_states(self) -> set[any]:
        return self._state

    def get_destinations(self) -> dict[(any, any), any]:
        return self._dest

    def get_sources(self) -> dict[(any, any), any]:
        return self._sources

    def get_destination(self, src: any, lbl: any) -> set[any]:
        if isinstance(lbl, set):
            return { j for i in lbl for j in self._dest.get((src, i), set())}
        return self._dest[(src, lbl)]

    def get_sources(self, lbl: any, dst: any) -> any:
        return self._sources[(lbl, dst)]
