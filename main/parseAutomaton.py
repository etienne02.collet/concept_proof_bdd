
from baseStruct.Automaton import *


auto = "A = Automaton()\n"
A = Automaton()
lst = []
print("input line after line all edge at your automaton with format : (src, dst, label)")
while True:
    input1 = input()
    if input1 == "":
        break
    val = tuple(input1.split())
    if len(val) != 3:
        print("value ignored")
        continue
    print("added : " + str(val))
    lst.append(val)

A.set_graphe(lst)
auto += "A.set_graphe(" + str(lst) + ")\n"
print(A.get_alphabet())
print("your state are : " + str(A.get_states()))

print("select initial : ")
input1 = input()
if input1 == "":
    print("it's not possible to no have inital state")
val = input1.split()
A.set_initial(val)
auto += "A.set_initial(" + str(val) + ")\n"

print("select final : ")
input1 = input()
if input1 == "":
    print("it's not possible to no have final state")
val = input1.split()
A.set_final(val)
auto += "A.set_final(" + str(val) + ")\n"

print("")
print("\n code : \n\n" + auto)