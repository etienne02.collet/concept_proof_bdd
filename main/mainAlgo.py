from networkx import MultiDiGraph
from copy import *

from main.baseStruct.Walk import *
from main.baseStruct.Annotation import *
from main.baseStruct.Automaton import *

def DOM(m: dict[any, any]) -> set[any]:
    return m.keys()


def Black_box(A: Annotation, l: int, S: set[any], v: any) -> set[any]:
    res = set()
    for q in S :
        i = A.get_val_for_length(v, q, l)
        res |= i.keys()
    return res

def Enumerate(A: Annotation, l: int, suffix: Walk, S: set[any]) -> None:
    v = suffix.source()
    if l == 0:
        print(str(suffix))
    else:
        for e in Black_box(A, l, S, v):
            S_bis = set()
            for q in S:
                s, states = A.get_val_for_edge(v, q, l, e)
                S_bis |= states
            Enumerate(A, l - 1, suffix.add(e, s), S_bis)
            suffix.supr()


def Annotate(Q: Automaton, D: MultiDiGraph, s: any, t: any) -> tuple[Annotation, int]:
    current = set()
    next = set()
    A = Annotation()
    l = 0
    for v in D.nodes():
        A.init_node(v)
    for q in Q.get_initial():
        A.add_val(s, q, (None, 0))
        next.add((s, q))
    while len(next) != 0 and Q.get_final().intersection(A.get_dom_to_node(t)) == set():
        current = next
        next = set()
        for v, q in current:
            for s, w, e, d in D.edges(nbunch=v, keys=True, data=True):
                for p in Q.get_destination(q, d["label"]):
                    tmp = A.get_val(w, p)
                    (S, x) = tmp if tmp != None else (set(), l + 1)
                    if x == l + 1:
                        A.add_val(w, p, (S | {((s, e), q)}, l + 1))
                        next.add((w, p))
        l += 1
    return A, l

def main_prog (Q: Automaton, D: MultiDiGraph, s, t) -> None:
    """
    main of program for enumerate all smallest walk
    """
    (A, l) = Annotate(Q, D, s, t)
    print("Annotate : \n" + str(A))
    print("l = " + str(l))
    S = set()
    for q in filter(lambda x : A.get_val(t, x)[1] == l, A.get_dom_to_node(t)):
        S.add(q)
    A.revert()
    print("S = " + str(S))
    Enumerate(A, l, Walk(t), S.intersection(Q.get_final()))