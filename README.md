# Base de donnée graphe

## Description

Ce projet, est un projet de découverte de la recherche. Ainsi, son objectif est de découvrir et d'apprendre la lecture et l'écriture d'article de recherche, ainsi que la méthodologie de celle-ci.

Celui-ci porte sur les bases de données orientées graphe. Lors de la recherche de résultats dans ce type de base de données, le nombre de résultats peut très facilement être infini ou exponentiel. Ainsi, il est nécessaire de placer un critère de sélection des résultats. Entre autres pour ce projet, nous nous intéresserons au résultat selon le plus cours chemin.

Plus précisément, il se découpe en trois parties : l'édition d'un algorithme de calcul de solution selon le critère précédemment évoqué, les tests d'efficacité, la rédaction de la démonstration de sa complexité, et l'amélioration de la complexité de ce programme.

***

## Installation

A remplir

***

## Usage
A remplir

***

## Objectifs
A remplir

***

## Auteurs et remerciment
Etienne Collet : Etudiant en 1er année de master à l'IGM ( Institue Gaspard-Monge)

Encadrant :

- Olivier Curé    :
- Nadime Francis  :
- Claire David    :
- Victor Marsault :

***

## Licence
C'est un projet open source.

***

## Etat du project
Projet en cours de dévellopement.
