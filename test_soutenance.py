from networkx import *
from main.baseStruct.Automaton import *
from main.mainAlgo import main_prog

#test 1
G = MultiDiGraph()
V = {'A', 'B', 'C', 'D', 'E'}
E = [('A', 'C', '1', {'label': {'b'}}), ('A', 'C', '2', {'label': {'a', 'b'}}), ('A', 'B', '3', {'label': {'a'}}),
     ('C', 'B', '4', {'label': {'a'}}), ('B', 'C', '5', {'label': {'a'}}), ('B', 'E', '6', {'label': {'b'}}),
     ('E', 'D', '7', {'label': {'b'}}), ('C', 'D', '8', {'label': {'a'}}), ('C', 'E', '9', {'label': {'a', 'b'}})]
G.add_nodes_from( V )
G.add_edges_from( E )
#print(G.edges(data=True, keys=True))
#G.write_dot("./tst_dot.out")
#print(G.edges)

#exit(0)
A = Automaton()
A.set_graphe([('P', 'P', 'a'), ('P', 'P', 'b'), ('P', 'Q', 'b')])
A.set_initial(['P'])
A.set_final(['Q'])

main_prog(A, G, 'A', 'D')