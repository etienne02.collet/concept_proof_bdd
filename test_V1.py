from networkx import *
from main.baseStruct.Automaton import *
from main.mainAlgo import main_prog

#test 1
G = MultiDiGraph()
V = {'W', 'T', 'X', 'V', 'Y', 'S'}
E = [('S', 'V', '1', {'label': {'c'}}), ('S', 'W', '2', {'label': {'a'}}), ('S', 'X', '3', {'label': {'b'}}), ('X', 'S', '4', {'label': {'a'}}), ('X', 'Y', '5', {'label': {'a'}}), ('Y', 'T', '6', {'label': {'c'}}), ('W', 'V', '7', {'label': {'a'}}), ('W', 'T', '8', {'label': {'a'}}), ('V', 'T', '9', {'label': {'a'}})]
G.add_nodes_from( V )
G.add_edges_from( E )
#print(G.edges(data=True, keys=True))
#G.write_dot("./tst_dot.out")
#print(G.edges)

#exit(0)
A = Automaton()
A.set_graphe([('P', 'P', 'a'), ('P', 'Q', 'b'), ('Q', 'Q', 'a'), ('P', 'R', 'c'), ('R', 'R', 'a'), ('R', 'R', 'b'), ('R', 'R', 'c'), ('Q', 'R', 'c'), ('Q', 'R', 'b')])
A.set_initial(['P'])
A.set_final(['Q'])

main_prog(A, G, 'S', 'T')

#test 2
G = MultiDiGraph()
V = {'U', 'T', 'R', 'F', 'S'}
E = [('S', 'R', '1', {'label': {'a', 'b'}}), ('S', 'R', '2', {'label': {'a'}}), ('R', 'S', '3', {'label': {'c'}}), ('R', 'U', '4', {'label': {'c'}}), ('U', 'F', '5', {'label': {'a'}}), ('S', 'F', '6', {'label': {'a'}}), ('U', 'R', '7', {'label': {'a'}}), ('R', 'R', '8', {'label': {'a', 'c'}}), ('R', 'T', '9', {'label': {'b'}}), ('F', 'T', '10', {'label': {'b'}})]
G.add_nodes_from( V )
G.add_edges_from( E )
#print(G.edges(data=True, keys=True))

A = Automaton()
A.set_graphe([('P', 'Q', 'a'), ('Q', 'Q', 'a'), ('Q', 'L', 'c'), ('L', 'M', 'a'), ('M', 'M', 'a'), ('M', 'E', 'b')])
A.set_initial(['P'])
A.set_final(['E'])

main_prog(A, G, 'S', 'T')